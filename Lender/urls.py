"""Lender URL Configuration
"""
from django.urls import path , include

from .views import lender_profile

urlpatterns = [
    path('Profile/', lender_profile),
]
